/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Structures */
typedef struct item_t {
    unsigned int value;
    unsigned int weight;
} item_t;

/* Global variables */
static unsigned int knapsack_size;
static unsigned int nb_items;
static item_t * table;

/* Functions */
static inline int read_from_file(char filename[]) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        char line[1024];
        char * ptr = NULL;
        
        /* Ignore the 1st line */
        fgets(line, sizeof(line), file);
        knapsack_size = strtol(line, &ptr, 10);
        nb_items = strtol(ptr, &ptr, 10);
        table = malloc(nb_items * sizeof(item_t));
        if (table == NULL) {
            return -1;
        }
        unsigned int index = 0;
        while (fgets(line, sizeof(line), file)) {
            table[index].value = strtol(line, &ptr, 10);
            table[index].weight = strtol(ptr, &ptr, 10);
            index++;
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline int print_items(void) {
    unsigned int index = 0;
    for ( ; index < nb_items ; index++){
        printf("table[%d] = %d / %d\n", index, table[index].value, table[index].weight);
    }
    return 0;
}

static inline unsigned int max(unsigned int value_1, unsigned int value_2) {
    if (value_1 < value_2) {
        return value_2;
    } else {
        return value_1;
    }
}

static inline int resolve_knapsack_problem(void) {
    
    // Init
    unsigned int * solutions = malloc((knapsack_size + 1) * sizeof(unsigned int));
    if (!solutions) {
        return -1;
    }
    memset(solutions, 0, (knapsack_size + 1) * sizeof(unsigned int));

    // Generate the table
    int i = 0;
    for ( ; i < nb_items; i++) {
        int x = knapsack_size;
        for ( ; x >= table[i].weight ; x--) {
            solutions[x] = max(solutions[x], 
                               solutions[x - table[i].weight] + table[i].value);
        }
    }

    // Determine the items
    int size = knapsack_size;
    int last = nb_items - 1;
    unsigned int answers[nb_items];
    unsigned int nb_answers = 0;
    printf("Optimal value: %d\n", solutions[size]);

    return 0;
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }

    // Read the value
    if (read_from_file(argv[1]) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }

    // Print the items
    //print_items();

    // Dynamic programming
    if (resolve_knapsack_problem() < 0) {
        printf("Error while resolving the problem\n");
        return -1;
    }

    return 0;
}

