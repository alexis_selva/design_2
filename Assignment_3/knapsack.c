/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Structures */
typedef struct item_t {
    unsigned int value;
    unsigned int weight;
} item_t;

/* Global variables */
static unsigned int knapsack_size;
static unsigned int nb_items;
static item_t * table;

/* Functions */
static inline int read_from_file(char filename[]) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        char line[1024];
        char * ptr = NULL;
        
        /* Read the number of items and max size */
        fgets(line, sizeof(line), file);
        knapsack_size = strtol(line, &ptr, 10);
        nb_items = strtol(ptr, &ptr, 10);
        table = malloc(nb_items * sizeof(item_t));
        if (table == NULL) {
            return -1;
        }
        unsigned int index = 0;
        while (fgets(line, sizeof(line), file)) {
            table[index].value = strtol(line, &ptr, 10);
            table[index].weight = strtol(ptr, &ptr, 10);
            index++;
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline int print_items(void) {
    unsigned int index = 0;
    for ( ; index < nb_items ; index++){
        printf("table[%d] = %d / %d\n", index, table[index].value, table[index].weight);
    }
    return 0;
}

static inline unsigned int max(unsigned int value_1, unsigned int value_2) {
    if (value_1 < value_2) {
        return value_2;
    } else {
        return value_1;
    }
}

static inline int resolve_knapsack_problem(void) {
    
    // Init
    unsigned int ** solutions = malloc(nb_items * sizeof(unsigned int *));
    if (!solutions) {
        return -1;
    }
    int i = 0;
    for ( ; i < nb_items ; i++) {
        solutions[i] = malloc((knapsack_size + 1) * sizeof(unsigned int));
        if (!solutions[i]) {
            return -1;
        }
        memset(solutions[i], 0, (knapsack_size + 1) * sizeof(unsigned int));
    }

    int x = 0;
#if 0
    for ( ; x <= knapsack_size ; x++) {
        solutions[0][x] = 0;
    }
#endif

    // Generate the table
    for (i = 1 ; i < nb_items; i++) {
        for (x = 0 ; x <= knapsack_size ; x++) {
            if (table[i].weight <= x) {
                solutions[i][x] = max(solutions[i - 1][x], 
                                      solutions[i - 1][x - table[i].weight] + table[i].value);
            } else {
                solutions[i][x] = solutions[i - 1][x];
            }
        }
    }

    // Determine the items
    int size = knapsack_size;
    int last = nb_items - 1;
    unsigned int answers[nb_items];
    unsigned int nb_answers = 0;
    printf("Optimal value: %d\n", solutions[last][size]);
#if 0
    while ((last >= 0) && (size >= 0)) {
        while ((last >= 1) && (solutions[last][size] == solutions[last - 1][size])) {
            last--;
        }
        if (size < table[last].weight) {
            break;
        }
        answers[nb_answers++] = last + 1;
        size -= table[last].weight;
        last--;
    }
    for (last = nb_answers - 1 ; last >= 0 ; last--) {
        printf("%d, ", answers[last]);
    }
    printf("\nsize = %d\n", knapsack_size - size);
#endif

    return 0;
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }

    // Read the value
    if (read_from_file(argv[1]) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }

    // Print the items
    //print_items();

    // Dynamic programming
    if (resolve_knapsack_problem() < 0) {
        printf("Error while resolving the problem\n");
        return -1;
    }

    return 0;
}

