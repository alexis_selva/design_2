/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>


/* Constants */
#define MAX_SIZE_ARRAY  (50)
#define UNLIMITED       (1000000.0)


/* Structures */
typedef struct edge_t {
    unsigned int vertex;
    double length;
    struct edge_t * next;
} edge_t;

typedef struct position_t {
    double x;
    double y;
} position_t;

typedef struct vertex_t {
    position_t position;
    int nb;
    edge_t * first;
    edge_t * last;
} vertex_t;


/* Global variables */
static unsigned int nb_vertices;
static vertex_t vertices[MAX_SIZE_ARRAY];


/* Functions */
static inline int size(vertex_t * v){
    return v->nb;
}

static inline int is_empty(vertex_t * v) {
    return (v->first == NULL);
}

static inline int enqueue(vertex_t * v, unsigned int value, double length) {
    edge_t * old = v->last;
    v->last = malloc(sizeof(edge_t));
    if (v->last) {
        v->last->vertex = value;
        v->last->length = length;
        v->last->next = NULL;
        if (is_empty(v)) { 
            v->first = v->last;
        } else {
            old->next = v->last;
        }
        v->nb++;
        return 0;
    }
    return -1;
}

static inline int dequeue(vertex_t * v) {
    if (is_empty(v)) {
        return -1;
    }
    int vertex = v->first->vertex;
    v->first = v->first->next;
    v->nb--;
    if (is_empty(v)) {
        v->last = NULL;
    }
    return vertex;
}

static inline int read_array_from_file(char filename[]) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        char line[1024];
        char * ptr = NULL;

        /* Read the number of vertices */
        fgets(line, sizeof(line), file);
        nb_vertices = strtol(line, &ptr, 10);

        /* Read the vertices and the corresponding edges */
        unsigned int count = 0;
        while (fgets(line, sizeof(line), file) && count < nb_vertices) {
            ptr = &line[0];
            vertex_t * vertex = &vertices[count++];
            vertex->position.x = strtod(line, &ptr);
            vertex->position.y = strtod(ptr, &ptr);
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline int print_graph(vertex_t array[MAX_SIZE_ARRAY]) {
    unsigned int i = 0;
    for (i = 0 ; i < nb_vertices ; i++) {
        vertex_t * v = &vertices[i];
        printf("%d (%f, %f): ", i, v->position.x, v->position.y);
        edge_t * e = v->first;
        while (e) {
            printf("%d,%d", e->vertex + 1, e->length);
            e = e->next;
        }
        printf("\n\n");
    }
    return 0;
}

static inline double euclidean_distance(vertex_t * v1, vertex_t * v2) {
    return sqrt(pow(v1->position.x - v2->position.x, 2) + pow(v1->position.y - v2->position.y, 2));
}

static inline int build_graph(void) {
    unsigned int i = 0;
    for ( ; i < nb_vertices ; i++) {
        unsigned int j = i + 1;
        for ( ; j < nb_vertices ; j++) {
            vertex_t * v1 = &vertices[i];
            vertex_t * v2 = &vertices[j];
            double distance = euclidean_distance(v1, v2);
            if (enqueue(v1, j, distance) < 0) {
                printf("Error while enqueing %d %d %f\n", i, j, distance);
                return -1;
            }
            if (enqueue(v2, i, distance) < 0) {
                printf("Error while enqueing %d %d %f\n", j, i, distance);
                return -1;
            }
        }

    }
    return 0;
}

static inline double min(double v, double w) {
    if (v < w) {
        return v;
    } else {
        return w;
    }
}

void shuffle(unsigned int *array, unsigned int nb) {
    if (nb > 1) {
        unsigned int i = 0;
        for ( ; i < nb - 1; i++) {
          unsigned int j = i + rand() / (RAND_MAX / (nb - i) + 1);
          unsigned int tmp = array[j];
          array[j] = array[i];
          array[i] = tmp;
        }
    }
}

static inline print_route(unsigned int *route, unsigned int nb) {
    unsigned int position = 0;
    for ( ; position < nb_vertices + 1; position++) {
        printf("%d ", route[position]);
    }
    printf("\n");
}

static inline init_route(unsigned int *route, unsigned int nb) {
    unsigned int position = 0;
    for ( ; position < nb_vertices ; position++) {
        route[position] = position; 
    }
    route[nb_vertices] = 0;
    //print_route(route, nb);
    unsigned random = 10;
    do {
        shuffle(route + 1, nb_vertices - 1);
    } while (random--);
    //print_route(route, nb);
}

static inline double calculate_route(unsigned int *route, unsigned int nb) {
    double distance = 0;
    unsigned int position = 1;
    for ( ; position < nb ; position++) {
        unsigned int source = route[position - 1];
        unsigned int destination = route[position];
        vertex_t * vertex = &vertices[source];
        edge_t *edge = vertex->first;
        while ((edge) && (edge->vertex != destination)) {
            edge = edge->next;
        }
        if (!edge) {
            print_route(route, nb);
            printf("error %d %d %d\n", position, source, destination);
        }
        distance += edge->length;
    }
    return distance;
}

static inline unsigned int two_opt_swap(unsigned int *new_route, 
                                        unsigned int *old_route,
                                        unsigned int limit_1, 
                                        unsigned int limit_2,
                                        unsigned int nb) {
    unsigned int position = 0;
    for ( ; position < limit_1 ; position++) {
        new_route[position] = old_route[position];
    }
    for ( ; position <= limit_2 ; position++) {
        new_route[position] = old_route[limit_2 - position + limit_1];
    }
    for ( ; position < nb ; position++) {
        new_route[position] = old_route[position];
    }
    //print_route(old_route, nb);
    //print_route(new_route, nb);
    return 0;
}
    
static inline double two_opt(unsigned int *existing_route, unsigned int nb) {
#define MAX_TRIES   1000
    double best_distance = 0;
    unsigned int improved = 0;
    best_distance = calculate_route(existing_route, nb);
    unsigned int i = 0;
    start_again:
    for (i = 1 ; i < nb - 2 ; i++) {
        unsigned int k = i + 1;
        for ( ; k < nb - 1 ; k++) {
            unsigned int new_route[nb];
            two_opt_swap(new_route, existing_route, i, k, nb);
            double new_distance = calculate_route(new_route, nb);
            if (new_distance < best_distance) {
                memcpy(existing_route, new_route, nb * sizeof(unsigned int));
                best_distance = new_distance;
                goto start_again;
            }
        }
    }
    return best_distance;
}

static inline double resolve_tsp(void) {
#define MAX_NO_IMPROVEMENTS 1000
    srand(time(NULL));
    double best_distance = UNLIMITED;
    unsigned int count = 0;
    while (count < MAX_NO_IMPROVEMENTS) {
        unsigned int random_route[nb_vertices + 1];
        init_route(random_route, nb_vertices + 1);
        double distance = two_opt(random_route, nb_vertices + 1);
        if (distance < best_distance) {
            best_distance = distance;
            count = 0;
            printf("best_distance = %f\n", best_distance);
        } else {
            count++;
            if (count % (MAX_NO_IMPROVEMENTS / 10) == 0) {
                printf("=> OK for %d%\n", count * 100 / MAX_NO_IMPROVEMENTS);
            }
        }
    }
    return best_distance;
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }
    if (read_array_from_file(argv[1]) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }
    
    if (build_graph() != 0) {
        printf("Error while building the graph\n");
        return -1; 
    }
    
    double distance = resolve_tsp();
    printf("Min TSP = %f\n", distance);

    return 0;
}

