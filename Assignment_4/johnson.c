/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Constants */
#define MAX_VERTICES   (1000)
#define MAX_SIZE_ARRAY (MAX_VERTICES + 1)
#define VIRTUAL_INDEX  (MAX_VERTICES)
#define UNLIMITED       100000


/* Structures */
typedef struct edge_t {
    unsigned int vertex;
    int length;
    struct edge_t * next;
} edge_t;

typedef struct vertex_t {
    int nb;
    edge_t * first;
    edge_t * last;
    int distance;
    unsigned char explored;
} vertex_t;

typedef struct heap_t {
    vertex_t * vertices[MAX_VERTICES];
    int nb;
} heap_t;


/* Global variables */
static unsigned int nb_vertices;
static vertex_t vertices[MAX_SIZE_ARRAY];
static heap_t heap;
static int ** solutions;


/* Functions */
static inline int size(vertex_t * v){
    return v->nb;
}

static inline int is_empty(vertex_t * v) {
    return (v->first == NULL);
}

static inline int enqueue(vertex_t * v, unsigned int value, int length) {
    edge_t * old = v->last;
    v->last = malloc(sizeof(edge_t));
    if (v->last) {
        v->last->vertex = value;
        v->last->length = length;
        v->last->next = NULL;
        if (is_empty(v)) { 
            v->first = v->last;
        } else {
            old->next = v->last;
        }
        v->nb++;
        return 0;
    }
    return -1;
}

static inline int dequeue(vertex_t * v) {
    if (is_empty(v)) {
        return -1;
    }
    int vertex = v->first->vertex;
    v->first = v->first->next;
    v->nb--;
    if (is_empty(v)) {
        v->last = NULL;
    }
    return vertex;
}

static inline int read_array_from_file(char filename[]) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        char line[1024];
        char * ptr = NULL;

        /* Read the number of vertices */
        fgets(line, sizeof(line), file);
        nb_vertices = strtol(line, &ptr, 10);

        /* Read the vertices and the corresponding edges */
        while (fgets(line, sizeof(line), file)) {
            long row = strtol(line, &ptr, 10);
            long vertex = strtol(ptr, &ptr, 10);
            long length = strtol(ptr + 1, &ptr, 10);
            for ( ; ptr && vertex != 0L ; vertex = strtol(ptr, &ptr, 10) , 
                                          length = strtol(ptr + 1, &ptr, 10)) {
                if (enqueue(&vertices[row - 1], vertex - 1, length) != 0) {
                    printf("Error while enqueuing: %d -> %d (%d) \n", row - 1, vertex - 1, length);
                    exit(1);
                }
            }
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline int print_graph(vertex_t array[MAX_SIZE_ARRAY]) {
    unsigned int i = 0;
    for (i = 0 ; i < nb_vertices ; i++) {
        vertex_t * v = &vertices[i];
        printf("%d (%d): ", i + 1, v->distance);
        edge_t * e = v->first;
        while (e) {
            printf("%d,%d", e->vertex + 1, e->length);
            e = e->next;
        }
        printf("\n\n");
    }
    return 0;
}

static inline int print_heap(vertex_t array[MAX_VERTICES]) {
    unsigned int i = 0;
    for (i = 0 ; i < heap.nb ; i++) {
        printf("%d: %d\n", i, heap.vertices[i]->distance);
    }
    return 0;
}

static inline void swap(int i, int j) {
    vertex_t * swap = heap.vertices[i];
    heap.vertices[i] = heap.vertices[j];
    heap.vertices[j] = swap;
}

static inline int sink(int k, int nb) {
    while (2 * k <= nb) {
        int j = 2 * k;
        if ((j < nb) && (heap.vertices[j - 1]->distance < heap.vertices[j]->distance)) {
            j++;
        }
        if (heap.vertices[k - 1]->distance >= heap.vertices[j - 1]->distance) {
            break;
        }
        swap(k - 1, j - 1);
        k = j;
    }
    return 0;
}

static inline int heapify(void) {
    int nb = heap.nb;
    int k = 0;
    for (k = nb/2; k >= 1; k--) {
        sink(k, nb);
    }
    while (nb > 1) {
        swap(0, nb - 1);
        nb--;
        sink(1, nb);
    }
    return 0;
}

static inline int initialize_heap(void) {
    int i = 0;
    for ( ; i < nb_vertices ; i++) {
        heap.vertices[i] = &vertices[i];
        heap.vertices[i]->distance = UNLIMITED;
        heap.vertices[i]->explored = 0;
    }
    heap.nb = nb_vertices;
    return 0;
}

static inline vertex_t * extract_heap(void) {
    vertex_t * v = heap.vertices[0];
    heap.nb--;
    heap.vertices[0] = heap.vertices[heap.nb];
    heap.vertices[heap.nb] = NULL;
    heapify();
    v->explored = 1;
    return v;
}

static inline int min(int v, int w) {
    if (v < w) {
        return v;
    } else {
        return w;
    }
}

static inline int dijkstra(unsigned int source) {

    printf("Run Dijkstra on %d\n", source);

    // Initialize the distance to the other vertices
    initialize_heap();

    // Determine the vertex corresponding to the source
    vertex_t * v = &vertices[source];

    // Set the computed shortest path distance
    v->distance = 0;

    // Loop
    while (heap.nb > 0) {

        // Get the min from the heap
        v = extract_heap();
        
        // Among all edges of v
        edge_t * e = v->first;
        while (e) {
            
            // Identify the connected vertex
            vertex_t * w = &vertices[e->vertex];
            
            // Update the distance of the vertex
            if (w->explored == 0) {
                w->distance = min(w->distance, v->distance + e->length);
            }

            // Get the next one
            e = e->next;
        }
        heapify();
    }
}

static inline int bellman_ford(unsigned int source) {
#define N   (nb_vertices + 1)
#define V   (nb_vertices + 1)

    // Init
    solutions = malloc(N * sizeof(int *));
    if (!solutions) {
        return -1;
    }
    unsigned int i = 0;
    unsigned int v = 0;
    for ( ; i < N ; i++) {
        solutions[i] = malloc(V * sizeof(int));
        if (!solutions[i]) {
            return -1;
        }
        solutions[0][i] = UNLIMITED;
    }
    solutions[0][source] = 0;
    
    // Generate the table
    for (i = 1 ; i < N ; i++) {
        for (v = 0 ; v < V ; v++) {
            unsigned int w = 0;
            int min_value = UNLIMITED;
            for (w = 0 ; w < V ; w++) {
                if (w != v) {
                    // Determine the vertex corresponding to w
                    vertex_t * vertex = &vertices[w];

                    // Determine the min edge length among all edges from w to v
                    edge_t * edge = vertex->first;
                    while (edge) {
                        if (edge->vertex == v) {
                            min_value = min(min_value, solutions[i - 1][w] + edge->length);
                            break;
                        }
                        // Get the next one
                        edge = edge->next;
                    }
                }
            }
            solutions[i][v] = min(solutions[i - 1][v], min_value);
        }
    }

    // Verify non negative loop
    for (v = 0 ; v < V ; v++) {
        if (solutions[N - 2][v] > solutions[N - 1][v]) {
            return -1;
        }
    }
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }
    if (read_array_from_file(argv[1]) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }

    // Step 1 add a new vertex to apply Johnson
    int v = 0;
    for ( ; v < nb_vertices ; v++) {
        if (enqueue(&vertices[VIRTUAL_INDEX], v, 0) != 0) {
            printf("Unable to add a new vertex for Johnson\n");
            return -1;
        }
    }
    //print_graph(vertices);
   
    // Step 2 run Bellman-Ford algorithm from the new vertex
    if (bellman_ford(VIRTUAL_INDEX) < 0) {
        printf("Negative loop detected !\n");
        return -1;
    }

    // Step 3 reweight 
    for (v = 0 ; v < nb_vertices ; v++) {
            
        // Determine the vertex corresponding to v
        vertex_t * vertex = &vertices[v];

        // Determine the edge
        edge_t * edge = vertex->first;
        while (edge) {

            // Update the length
            edge->length = edge->length + solutions[nb_vertices][v] 
                                        - solutions[nb_vertices][edge->vertex];

            // Get the next one
            edge = edge->next;
        }
    }

    // Step 4 run Dijkstra algorithm
    unsigned int v1 = 0;
    int distance = UNLIMITED;
    unsigned int src = v1;
    unsigned int dst = v1;
    for (v1 = 0 ; v1 < nb_vertices ; v1++) {
        dijkstra(v1);

        // Step 5 determine the min path by taking into account the updated distance
        unsigned int v2 = 0;
        for (v2 = 0 ; v2 < nb_vertices ; v2++) {
            if (v2 != v1) {
                distance = min(distance, vertices[v2].distance - solutions[nb_vertices][v1] 
                                                               + solutions[nb_vertices][v2]);
            }
        }        
    }

    printf("Min distance : %d\n", distance);

    return 0;
}

