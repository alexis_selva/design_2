/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Constants */
#define MAX_VERTICES   1000000
#define MAX_SIZE_ARRAY (2 * MAX_VERTICES)

typedef enum lecture_t {
    NORMAL = 0,
    INVERSED = 1,
    SORTED = 2,
} lecture_t;


/* Structures */
typedef struct edge_t {
    unsigned int vertex;
    struct edge_t * next;
} edge_t;

typedef struct queue_t {
    unsigned char explored;
    int nb;
    edge_t * first;
    edge_t * last;
} queue_t;


/* Global variables */
static unsigned int nb_vertices;
static queue_t vertices[MAX_SIZE_ARRAY];
static queue_t inv_vertices[MAX_SIZE_ARRAY];
static int leaders[MAX_SIZE_ARRAY];
static int leaders_nb[MAX_SIZE_ARRAY];
static int times[MAX_SIZE_ARRAY];
static int finish_time;
static int source;

/* Functions */
static inline int size(queue_t * q){
    return q->nb;
}

static inline int is_empty(queue_t * q) {
    return (q->first == NULL);
}

static inline int enqueue(queue_t array[MAX_SIZE_ARRAY], int v1, int v2) {
    queue_t * q = &array[v1];
    edge_t * old = q->last;
    q->last = malloc(sizeof(edge_t));
    if (q->last) {
        q->last->vertex = v2;
        q->last->next = NULL;
        if (is_empty(q)) { 
            q->first = q->last;
        } else {
            old->next = q->last;
        }
        q->nb++;
        return 0;
    }
    return -1;
}

static inline void normalize(int *value) {
    if (*value < 0) {
        *value = -*value;
    } else {
        *value = *value + nb_vertices;
    }
}

static inline int inverse(int value) {
    if (value > nb_vertices) {
        return value - nb_vertices;
    } else {
        return value + nb_vertices;
    }
}

static inline int read_array_from_file(char filename[], lecture_t type) {
    FILE * file = fopen(filename, "r");
    if (file) {
        char line[1024];
        char * ptr = NULL;
        fgets(line, sizeof(line), file);
        nb_vertices = strtol(line, &ptr, 10);
        while (fgets(line, sizeof(line), file)) {
            int x1 = strtol(line, &ptr, 10);
            normalize(&x1);
            int x2 = strtol(ptr, &ptr, 10);
            normalize(&x2);
            //printf("%d %d\n", x1, x2);
            switch (type) {

                case INVERSED:
                    {
                        // INV -x1 -> x2 => x2 -> -x1
                        if (enqueue(inv_vertices, x2 - 1, inverse(x1) - 1) != 0) {
                            printf("Error while enqueuing: %d -> %d\n", x2, -x1);
                            exit(1);
                        }
                        // INV -x2 -> x1 => x1 -> -x2
                        if (enqueue(inv_vertices, x1 - 1, inverse(x2) - 1) != 0) {
                            printf("Error while enqueuing: %d -> %d\n", x1, -x2);
                            exit(1);
                        }
                    }
                    break;

                case SORTED:
                    {
                        // -x1 -> x2
                        if (enqueue(vertices, times[inverse(x1) - 1], times[x2 - 1]) != 0) {
                            printf("Error while enqueuing after sorting: %d -> %d\n", -x1, x2);
                            exit(1);
                        }
                        // -x2 -> x1
                        if (enqueue(vertices, times[inverse(x2) - 1], times[x1 - 1]) != 0) {
                            printf("Error while enqueuing after sorting: %d -> %d\n", -x2, x1);
                            exit(1);
                        }
                    }
                    break;
            }
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline int depth_first_search(queue_t array[MAX_SIZE_ARRAY], int index) {
    leaders[index]= source;
    leaders_nb[source] += 1;
    queue_t * q = &array[index];
    q->explored = 1;
    edge_t * e = q->first;
    while (e) {
        if (!array[e->vertex].explored) {
            depth_first_search(array, e->vertex);
        }
        e = e->next;
    }
    times[index] = finish_time;
    finish_time++;
}

static int depth_first_search_loop(queue_t array[MAX_SIZE_ARRAY]) {
    source = 0;
    finish_time = 0;
    memset(times, 0, MAX_SIZE_ARRAY * sizeof (int));
    memset(leaders, 0, MAX_SIZE_ARRAY * sizeof(int));
    memset(leaders_nb, 0, MAX_SIZE_ARRAY * sizeof(int));
    int index = 0;
    for (index = 2 * nb_vertices - 1 ; index >= 0; index--) {
        queue_t * q = &array[index]; 
        if (!q->explored) {
            source = index;
            depth_first_search(array, index);
        }
    }
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }
    if (read_array_from_file(argv[1], INVERSED) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }
   
    // DFS on reversed graph
    depth_first_search_loop(inv_vertices);
    int copy_times[MAX_SIZE_ARRAY];
    memcpy(copy_times, times, MAX_SIZE_ARRAY * sizeof(int));

    // Reorder by considering the finished times
    if (read_array_from_file(argv[1], SORTED) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }
    

    // DFS on graph
    depth_first_search_loop(vertices);

    // Test if a unique SCC exists
    unsigned int i = 0;
    for ( ; i < nb_vertices ; i++) {
        if ((leaders[copy_times[i]] == leaders[copy_times[i + nb_vertices]])) {
            printf("KO for i == %d and leaders[%d] == leaders[%d] == %d\n", i, 
                                                                            copy_times[i], 
                                                                            copy_times[i + nb_vertices], 
                                                                            leaders[copy_times[i]]);
            return -1;
        }
    }
    printf("OK\n");

    return 0;
}

