/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Structure */
typedef struct job_t {
    int weight;
    int length;
    int completion_time;
} job_t;

typedef struct jobs_t {
    unsigned int nb;
    job_t list[];
} jobs_t;


/* Functions */
static inline int read_jobs(char filename[], jobs_t ** jobs) {
    
    /* Open the file */
    FILE * file = fopen(filename, "r");
    if (!file) {
        printf("Error while opening the file: %s\n", filename);
        return -1;
    }

    /* Read the number of jobs */
    char line[1024];
    if (!fgets(line, sizeof(line), file)) {
        printf("Error while getting the 1st line\n");
        return -1;
    }
    char * ptr = NULL;
    long nb = strtol(line, &ptr, 10);
    if (nb <= 0) {
        printf("Error while reading the number of jobs: %d\n", nb);
        return -1;
    }

    /* Allocate the job array */
    *jobs = malloc(sizeof(unsigned int) + sizeof(job_t) * nb);
    if (jobs == NULL) {
        printf("Error while allocating the jobs\n");
        return -1;
    }
    (*jobs)->nb = nb;

    /* Read the job info */
    unsigned int i= 0;
    while (fgets(line, sizeof(line), file)) {
        ptr = NULL;
        (*jobs)->list[i].weight = strtol(line, &ptr, 10);
        (*jobs)->list[i].length = strtol(ptr + 1, &ptr, 10);
        i++;
    }

    /* Close the file */
    fclose(file);
    return 0;
}

static inline int print_jobs(jobs_t * jobs) {
    unsigned int i = 0;
    for ( ; i < jobs->nb ; i++) {
        printf("Job %d: weight = %d, length %d, end = %d\n", i, jobs->list[i].weight, 
                                                            jobs->list[i].length,
                                                            jobs->list[i].completion_time);
    }
    return 0;
}

static inline int copy(const job_t * src, job_t * dst) {
    dst->weight = src->weight;
    dst->length = src->length;
    return 0;
}

static inline int swap(job_t * job_1, job_t * job_2) {
    job_t tmp;
    copy(job_1, &tmp);
    copy(job_2, job_1);
    copy(&tmp, job_2);
    return 0;
}

#if 1
static inline int compare(const job_t * job_1, const job_t * job_2) {
    int diff_1 = job_1->weight - job_1->length;
    int diff_2 = job_2->weight - job_2->length;
    if (diff_1 < diff_2) {
        return -1;
    } else if (diff_1 > diff_2) {
        return +1;
    } else {
        return (job_1->weight - job_2->weight);
    }
}
#endif

#if 0
static inline int compare(const job_t * job_1, const job_t * job_2) {
    float diff_1 = job_1->weight * 1.0 / job_1->length;
    float diff_2 = job_2->weight * 1.0 / job_2->length;
    if (diff_1 < diff_2) {
        return -1;
    } else if (diff_1 > diff_2) {
        return +1;
    } else {
        return (job_1->weight - job_2->weight);
    }
}
#endif

static inline int partition(job_t input[], unsigned int * pivot, 
                                           unsigned int left, 
                                           unsigned int right) {
    int i = left + 1;
    int j = i;
    for (j = i ; j <= right ; j++) {
        if ((j != *pivot) && (compare(&input[j], &input[*pivot]) > 0)) {
            swap(&input[i], &input[j]);
            i++;
        }
    }
    swap(&input[*pivot], &input[i - 1]);
    *pivot = i - 1;
    return 0;
}

static inline int quicksort(job_t input[], unsigned int left, unsigned int right) {
    if (right <= left) {
        return 0;
    }
    unsigned int pivot = left;
    partition(input, &pivot, left, right);
    quicksort(input, left, pivot - 1);
    quicksort(input, pivot + 1, right);
    return 0;
}

static inline int update_completion_time(jobs_t * jobs) {
    unsigned int i = 0;
    int previous_completion_time = 0;
    for ( ; i < jobs->nb ; i++) {
        previous_completion_time += jobs->list[i].length;
        jobs->list[i].completion_time = previous_completion_time;
    }
    return 0;
}

static inline unsigned long long sum_weighted_completion_times(jobs_t * jobs) {
    unsigned int i = 0;
    unsigned long long result = 0;
    for ( ; i < jobs->nb ; i++) {
        result += (jobs->list[i].weight * jobs->list[i].completion_time);
    }
    return result;
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }
    jobs_t * jobs = NULL;
    if (read_jobs(argv[1], &jobs) != 0) {
        return -1; 
    }
    //print_jobs(jobs);
    
    /* Run quicksort to sort the jobs */
    quicksort(jobs->list, 0, jobs->nb - 1);
    update_completion_time(jobs);
    //print_jobs(jobs);
   
    /* Print the result */
    printf("Result = %lld\n", sum_weighted_completion_times(jobs));

    /* Free the jobs */
    free(jobs);
    return 0;
}

