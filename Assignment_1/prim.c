/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Constants */
#define MAX_SIZE_ARRAY 500
#define UNLIMITED      (100000)


/* Structures */
typedef struct edge_t {
    unsigned int vertex;
    int length;
    struct edge_t * next;
} edge_t;

typedef struct vertex_t {
    int nb;
    edge_t * first;
    edge_t * last;
    int distance;
    unsigned char explored;
} vertex_t;

typedef struct heap_t {
    vertex_t * vertices[MAX_SIZE_ARRAY];
    int nb;
} heap_t;


/* Global variables */
static vertex_t vertices[MAX_SIZE_ARRAY];
static heap_t heap;


/* Functions */
static inline int size(vertex_t * v){
    return v->nb;
}

static inline int is_empty(vertex_t * v) {
    return (v->first == NULL);
}

static inline int enqueue(vertex_t * v, unsigned int value, int length) {
    edge_t * old = v->last;
    v->last = malloc(sizeof(edge_t));
    if (v->last) {
        v->last->vertex = value;
        v->last->length = length;
        v->last->next = NULL;
        if (is_empty(v)) { 
            v->first = v->last;
        } else {
            old->next = v->last;
        }
        v->nb++;
        return 0;
    }
    return -1;
}

static inline int dequeue(vertex_t * v) {
    if (is_empty(v)) {
        return -1;
    }
    unsigned int vertex = v->first->vertex;
    v->first = v->first->next;
    v->nb--;
    if (is_empty(v)) {
        v->last = NULL;
    }
    return vertex;
}

static inline int read_array_from_file(char filename[]) {
    FILE * file = fopen(filename, "r");
    if (file) {
        char line[1024];
        
        /* Ignore the 1st line */
        fgets(line, sizeof(line), file);

        while (fgets(line, sizeof(line), file)) {
            char * ptr = NULL;
            unsigned int row = strtol(line, &ptr, 10);
            unsigned int vertex = strtol(ptr, &ptr, 10);
            int length = strtol(ptr + 1, &ptr, 10);
            if (enqueue(&vertices[row - 1], vertex - 1, length) != 0) {
                printf("Error while enqueuing: %d -> %d (%d) \n", row - 1, vertex - 1, length);
                return -1;
            }
            if (enqueue(&vertices[vertex - 1], row - 1, length) != 0) {
                printf("Error while enqueuing: %d -> %d (%d) \n", vertex - 1, row - 1, length);
                return -1;
            }
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline int print_graph(void) {
    unsigned int i = 0;
    for (i = 0 ; i < MAX_SIZE_ARRAY ; i++) {
        printf("%d: ", i);
        vertex_t * v = &vertices[i];
        edge_t * e = v->first;
        while (e) {
            printf("%d,%d ", e->vertex, e->length);
            e = e->next;
        }
        printf("\n\n");
    }
    return 0;
}

static inline int print_heap(void) {
    unsigned int i = 0;
    for (i = 0 ; i < heap.nb ; i++) {
        printf("%d: %d\n", heap.vertices[i] - &vertices[0], heap.vertices[i]->distance);
    }
    printf("\n");
    return 0;
}

static inline void swap(unsigned int i, unsigned int j) {
    vertex_t * swap = heap.vertices[i];
    heap.vertices[i] = heap.vertices[j];
    heap.vertices[j] = swap;
}

static inline int sink(unsigned int k, unsigned int nb) {
    while (2 * k <= nb) {
        unsigned int j = 2 * k;
        if ((j < nb) && (heap.vertices[j - 1]->distance < heap.vertices[j]->distance)) {
            j++;
        }
        if (heap.vertices[k - 1]->distance >= heap.vertices[j - 1]->distance) {
            break;
        }
        swap(k - 1, j - 1);
        k = j;
    }
    return 0;
}

static inline int heapify(void) {
    unsigned int nb = heap.nb;
    unsigned int k = 0;
    for (k = nb/2; k >= 1; k--) {
        sink(k, nb);
    }
    while (nb > 1) {
        swap(0, nb - 1);
        nb--;
        sink(1, nb);
    }
    return 0;
}

static inline int initialize_heap(void) {
    unsigned int i = 0;
    for ( ; i < MAX_SIZE_ARRAY ; i++) {
        heap.vertices[i] = &vertices[i];
        heap.vertices[i]->distance = UNLIMITED;
    }
    heap.nb = MAX_SIZE_ARRAY;
    return 0;
}

static inline vertex_t * extract_heap(void) {
    vertex_t * v = heap.vertices[0];
    heap.nb--;
    heap.vertices[0] = heap.vertices[heap.nb];
    heap.vertices[heap.nb] = NULL;
    heapify();
    v->explored = 1;
    return v;
}

static inline int prim(unsigned int source) {

    // Initialize the distance to the other vertices
    initialize_heap();

    // Determine the vertex corresponding to the source
    vertex_t * v = &vertices[source];
    v->distance = -UNLIMITED;
    heapify();

    // Loop
    int result = 0;
    while (heap.nb > 0) {

        // Get the min from the heap
        v = extract_heap();
        if (heap.nb < MAX_SIZE_ARRAY - 1) {
            result += v->distance;
        }
        
        // Among all edges of v
        edge_t * e = v->first;
        while (e) {
            
            // Identify the connected vertex
            vertex_t * w = &vertices[e->vertex];
            
            // Update the distance of the vertex
            if (w->explored == 0) {
                if (e->length < w->distance) {
                    w->distance = e->length;
                    heapify();
                }
            }
            
            // Get the next one
            e = e->next;
        }
#if 0
        printf("\n heap.nb = %d \n", heap.nb);
        print_heap();
#endif
    }
    return result;
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }
    int source = 0;
    if (argc == 3) {
        source = atoi(argv[2]);
    }
    if (read_array_from_file(argv[1]) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }
    //print_graph();
   
    // Run Prim's algorithm
    int cost = prim(source);

    // Evaluate the size of the minimum spanning tree
    printf("Result: %d\n", cost);

    return 0;
}

