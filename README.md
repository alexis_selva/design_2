These are the programming assignments relative to design (2nd part):

* Assignment 1: coding up the greedy algorithms for minimizing the weighted sum of completion times and scheduling jobs in decreasing order of the ratio
* Assignment 2: coding up the clustering algorithm for computing a max-spacing k-clustering
* Assignment 3: coding up the knapsack algorithm
* Assignment 4: implementing one or more algorithms for the all-pairs shortest-path problem
* Assignment 5: implementing one or more algorithms for the traveling salesman problem
* Assignment 6: implementing one or more algorithms for the 2SAT problem

For more information, I invite you to have a look at https://www.coursera.org/course/algo2