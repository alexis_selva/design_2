/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Constant */
#define MAX_NB_BUCKETS  100003 // prime number


/* Structures */
typedef struct entry_t {
    unsigned int key;
    unsigned int count;
    struct entry_t * leader;
    unsigned int leader_nb;
    struct entry_t * next;
} entry_t;

typedef struct hashtable_t {
    entry_t * buckets[MAX_NB_BUCKETS];
    unsigned int nb;
} hashtable_t;


/* Global variables */
static unsigned int nb_vertices;
static unsigned int nb_bits;


/* Functions */
static inline unsigned int size(hashtable_t * table){
    return table->nb;
}

static inline int is_empty(hashtable_t * table) {
    return size(table) == 0;
}

static inline unsigned int hash(unsigned int key) {
    unsigned int value = key + (key << 3) + (key << 12);
    return (value & 0x7fffffff) % MAX_NB_BUCKETS;
}

static inline entry_t * find(hashtable_t * table, unsigned int key) {
    
    // Determine the corresponding bucket
    unsigned int bucket = hash(key);

    // Lookup the key into the list
    entry_t * entry = table->buckets[bucket];
    while (entry) {
        if (entry -> key == key) {
            return entry;
        }
        entry = entry->next;
    }

    return NULL;
}

static inline entry_t * add(hashtable_t * table, long long int key) {

    // Is the key present into the table ?
    entry_t * found = find(table, key);
    if (found) {
        found->count++;
        return found;
    } else {

        // Allocate and insert a new entry at the head
        entry_t * new_entry = malloc(sizeof(entry_t));
        if (new_entry) {
            new_entry->key = key;
            new_entry->count = 0;
            new_entry->leader = new_entry;
            new_entry->leader_nb = 1;
            unsigned int bucket = hash(key);
            new_entry->next = table->buckets[bucket];
            table->buckets[bucket] = new_entry;
            table->nb++;
            return new_entry;
        }
        return NULL;
    }
}

void remove_spaces(char* source) {
    char* i = source;
    char* j = source;
    while(*j != 0) {
        *i = *j++;
        if(*i != ' ')
            i++;
    }
    *i = 0;
}

static inline int read_from_file(hashtable_t * table, char filename[]) {
    FILE * file = fopen(filename, "r");
    
    if (file) {
        char line[1024];
        char * ptr = NULL;
        
        /* Ignore the 1st line */
        fgets(line, sizeof(line), file);
        nb_vertices = strtol(line, &ptr, 10);
        nb_bits = strtol(ptr, &ptr, 10);
        while (fgets(line, sizeof(line), file)) {
            remove_spaces(line);
            unsigned int value = strtol(line, &ptr, 2);
            if (add(table, value) == NULL) {
                printf("Error while adding: %lld\n", value);
                return -1;
            }
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline unsigned int hamming(unsigned int value_1, unsigned int value_2) {
    return (1 << value_1 | 1 << value_2);
}

static inline unsigned int generate_key(unsigned int key, unsigned int value_1, unsigned int value_2) {
    return key ^ hamming(value_1, value_2);
}

static inline int update_leaders(hashtable_t *table, entry_t * leader_1, entry_t * leader_2) {
    if (leader_1->leader_nb <= leader_2->leader_nb) {
        unsigned int index = 0;
        for ( ; index < MAX_NB_BUCKETS ; index++) {
            entry_t * entry = table->buckets[index];
            while (entry) {
                if (entry->leader == leader_1) {
                    entry->leader = leader_2;
                }
                entry = entry->next;
            }
        }
#if 0
        printf("%d <- %d (%d + %d = %d)\n", leader_2->key, leader_1->key,
                                            leader_2->leader_nb, leader_1->leader_nb,
                                            leader_2->leader_nb + leader_1->leader_nb);
#endif
        leader_2->leader_nb += leader_1->leader_nb;
    } else {
        unsigned int index = 0;
        for ( ; index < MAX_NB_BUCKETS ; index++) {
            entry_t * entry = table->buckets[index];
            while (entry) {
                if (entry->leader == leader_2) {
                    entry->leader = leader_1;
                }
                entry = entry->next;
            }
        }
#if 0
        printf("%d <- %d (%d + %d = %d)\n", leader_1->key, leader_2->key,
                                            leader_1->leader_nb, leader_2->leader_nb,
                                            leader_1->leader_nb + leader_2->leader_nb);
#endif
        leader_1->leader_nb += leader_2->leader_nb;
    }
    return 0;
}


int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }

    // Initialize table
    hashtable_t table;
    memset(&table, 0, sizeof(hashtable_t));

    // Read the value
    if (read_from_file(&table, argv[1]) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }

    // Analyse the values
    hashtable_t cluster;
    memset(&cluster, 0, sizeof(hashtable_t));
    unsigned int index = 0;
    for ( ; index < MAX_NB_BUCKETS ; index++) {
        entry_t * entry = table.buckets[index];
        while (entry) {
            if (entry->count) {
                entry_t * entry_0 = add(&cluster, entry->key);
                entry_0->leader->leader_nb += entry->count;
            }
            unsigned int value_1 = 0;
            for ( ; value_1 < nb_bits ; value_1++) {
                unsigned int value_2 = 0;
                for ( ; value_2 <= value_1 ; value_2++) {
                    unsigned int key = generate_key(entry->key, value_1, value_2);
                    entry_t * found = find(&table, key);
                    if (found) {
                        entry_t * entry_1 = add(&cluster, key);
                        entry_t * entry_2 = add(&cluster, entry->key);
                        if (entry_1->leader != entry_2->leader) {
                            update_leaders(&cluster, entry_1->leader, entry_2->leader);
                        }
                    }
                }
            }
            entry = entry->next;
        }
    }

    // The end
    printf("\n");
    unsigned int nb_vertices_in_cluster = 0;
    unsigned int nb_clusters = 0;
    for ( index = 0 ; index < MAX_NB_BUCKETS ; index++) {
        entry_t * entry = cluster.buckets[index];
        while (entry) {
            if (entry->leader == entry) {
                nb_vertices_in_cluster += entry->leader_nb;
                nb_clusters++;
#if 0
                printf("%d (%d)\n",entry->key, entry->leader_nb);
#endif
            } else {
#if 0
                printf("%d (%d) -> %d (%d)\n",entry->key, entry->leader_nb
                                             ,entry->leader->key
                                             ,entry->leader->leader_nb);
#endif
            }
            entry = entry->next;   

        }
    }

    printf("Result = %d - %d + %d = %d\n", nb_vertices, nb_vertices_in_cluster, nb_clusters, 
                                           nb_vertices - nb_vertices_in_cluster + nb_clusters);
    return 0;
}

