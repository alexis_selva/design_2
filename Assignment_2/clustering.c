/* Dependancies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/* Constants */
#define MAX_SIZE_ARRAY 500
#define UNLIMITED      (100000)


/* Structures */
typedef struct edge_t {
    unsigned int vertex_1;
    unsigned int vertex_2;
    int length;
    struct edge_t * next;
} edge_t;

typedef struct vertex_t {
    edge_t * first;
    edge_t * last;
    struct vertex_t * leader;
    unsigned int leader_nb;
} vertex_t;

typedef struct heap_t {
    edge_t * edges[MAX_SIZE_ARRAY * MAX_SIZE_ARRAY];
    int nb;
} heap_t;


/* Global variables */
static unsigned int vertices_nb;
static vertex_t vertices[MAX_SIZE_ARRAY];
static heap_t heap;

/* Functions */
static inline void swap(unsigned int i, unsigned int j) {
    edge_t * swap = heap.edges[i];
    heap.edges[i] = heap.edges[j];
    heap.edges[j] = swap;
}

static inline int sink(unsigned int k, unsigned int nb) {
    while (2 * k <= nb) {
        unsigned int j = 2 * k;
        if ((j < nb) && (heap.edges[j - 1]->length < heap.edges[j]->length)) {
            j++;
        }
        if (heap.edges[k - 1]->length >= heap.edges[j - 1]->length) {
            break;
        }
        swap(k - 1, j - 1);
        k = j;
    }
    return 0;
}

static inline int heapify(void) {
    unsigned int nb = heap.nb;
    unsigned int k = 0;
    for (k = nb/2; k >= 1; k--) {
        sink(k, nb);
    }
    while (nb > 1) {
        swap(0, nb - 1);
        nb--;
        sink(1, nb);
    }
    return 0;
}

static inline int initialize_heap(void) {
    heap.nb = 0;
    return 0;
}

static inline int add_heap(edge_t *e) {
    heap.edges[heap.nb] = e;
    heap.nb++;
    return 0;
}

static inline edge_t * extract_heap(void) {
    edge_t * v = heap.edges[0];
    heap.nb--;
    heap.edges[0] = heap.edges[heap.nb];
    heap.edges[heap.nb] = NULL;
    heapify();
    return v;
}

static inline int is_empty(vertex_t * v) {
    return (v->first == NULL);
}

static inline int enqueue(unsigned int vertex_1, unsigned int vertex_2, int length) {
    vertex_t * v = &vertices[vertex_1];
    edge_t * old = v->last;
    v->last = malloc(sizeof(edge_t));
    if (v->last) {
        v->last->vertex_1 = vertex_1;
        v->last->vertex_2 = vertex_2;
        v->last->length = length;
        v->last->next = NULL;
        if (is_empty(v)) { 
            v->first = v->last;
        } else {
            old->next = v->last;
        }
        add_heap(v->last);
        return 0;
    }
    return -1;
}

static inline int initialize_vertices() {
    unsigned int index = 0;
    for ( ; index < MAX_SIZE_ARRAY ; index++) {
        vertex_t * vertex = &vertices[index];
        vertex->first = NULL;
        vertex->last = NULL;
        vertex->leader = vertex;
        vertex->leader_nb = 1;
    }
}

static inline int read_array_from_file(char filename[]) {
    FILE * file = fopen(filename, "r");
    if (file) {
        char line[1024];
        char * ptr = NULL;
        
        /* Ignore the 1st line */
        fgets(line, sizeof(line), file);
        vertices_nb = strtol(line, &ptr, 10);

        while (fgets(line, sizeof(line), file)) {
            unsigned int row = strtol(line, &ptr, 10);
            unsigned int vertex = strtol(ptr, &ptr, 10);
            int length = strtol(ptr + 1, &ptr, 10);
            if (enqueue(row - 1, vertex - 1, length) != 0) {
                printf("Error while enqueuing: %d -> %d (%d) \n", row - 1, vertex - 1, length);
                return -1;
            }
#if 0
            if (enqueue(vertex - 1, row - 1, length) != 0) {
                printf("Error while enqueuing: %d -> %d (%d) \n", vertex - 1, row - 1, length);
                return -1;
            }
#endif
        }
        fclose(file);
        return 0;
    }
    
    return -1;
}

static inline int print_graph(void) {
    unsigned int i = 0;
    for (i = 0 ; i < MAX_SIZE_ARRAY ; i++) {
        printf("%d: ", i);
        vertex_t * v = &vertices[i];
        edge_t * e = v->first;
        while (e) {
            printf("%d <---(%d)---> %d\n", e->vertex_1, e->length, e->vertex_2);
            e = e->next;
        }
        printf("\n\n");
    }
    return 0;
}

static inline int update_leader(vertex_t * leader_1, vertex_t * leader_2) {
    if (leader_1->leader_nb <= leader_2->leader_nb) {
        unsigned int i = 0;
        for ( ; i < MAX_SIZE_ARRAY ; i++) {
            if (vertices[i].leader == leader_1) {
                vertices[i].leader = leader_2;
            }
        }
        leader_2->leader_nb += leader_1->leader_nb;
    } else {
        unsigned int i = 0;
        for ( ; i < MAX_SIZE_ARRAY ; i++) {
            if (vertices[i].leader == leader_2) {
                vertices[i].leader = leader_1;
            }
        }
        leader_1->leader_nb += leader_2->leader_nb;
    }
    return 0;
}

static inline int kruskal(unsigned int clustering) {
    unsigned int index = 0;
    edge_t * e = NULL;

    // Loop
    while (index <= vertices_nb - clustering) {

        // Get the min from the heap
        e = extract_heap();
        
        // Update the leader invariant
        vertex_t * v1 = &vertices[e->vertex_1];
        vertex_t * v2 = &vertices[e->vertex_2];
#if 0
        printf("e->vertex_1 = %d / %d\n", e->vertex_1, v1->leader - &vertices[0]);
        printf("e->vertex_2 = %d / %d\n", e->vertex_2, v2->leader - &vertices[0]);
#endif
        if (v1->leader != v2->leader) { 
#if 0
            printf(" => update_leader %d (%d) %d (%d)\n", v1->leader_nb, v1->leader - &vertices[0],
                                                          v2->leader_nb, v2->leader - &vertices[0]);
#endif
            update_leader(v1->leader, v2->leader);
#if 0
            printf(" => update_leader %d (%d) %d (%d)\n", v1->leader_nb, v1->leader - &vertices[0],
                                                          v2->leader_nb, v2->leader - &vertices[0]);
#endif
            index++;
        }
    }

    // Get the next edge from the heap
    e = extract_heap();
    return e->length;
}

int main (int argc, char * argv[]) {
    if (argc < 2) {
        printf("No specified filename\n");
        return -1;
    }
    int source = 0;
    if (argc == 3) {
        source = atoi(argv[2]);
    }

    // Initialize the heap
    initialize_heap();

    // Initialize the vertices
    initialize_vertices();

    // Read the list of vertices/edges
    if (read_array_from_file(argv[1]) != 0) {
        printf("Error while reading %s\n", argv[1]);
        return -1; 
    }
    heapify();
   
    // Run Krushal's algorithm
    unsigned int res = kruskal(4);
    unsigned int index = 0;
    unsigned int max = 0;
    for ( ; index < MAX_SIZE_ARRAY ; index++) {
        if (max < vertices[index].leader_nb) {
            max = vertices[index].leader_nb;
        }
    }

    // Evaluate the maximum spacing
    printf("Result: %d\n", res);

    return 0;
}

